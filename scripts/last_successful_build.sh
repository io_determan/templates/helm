#!/bin/bash

set -e

# set vars in project settings
BASE_URL=${BASE_URL:-$(echo $CI_PROJECT_URL |  cut -d'/' -f1-3)}
PRIVATE_TOKEN=${PRIVATE_TOKEN:-$CI_JOB_TOKEN}
PROJECT=${PROJECT:-$CI_PROJECT_ID}
VARS=( BASE_URL PRIVATE_TOKEN PROJECT )

LAST_SUCCESSFUL_BUILD=$(curl -s -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${BASE_URL}/api/v4/projects/${PROJECT}/jobs?scope=success&per_page=1" | jq -c ''"$FILTER"' | {id}' | head -n1 | grep -oE '[0-9]+')

echo "$LAST_SUCCESSFUL_BUILD"